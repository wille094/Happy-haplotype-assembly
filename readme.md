# Overview

Below a pipeline is presented that provides both methods to:
- manipulate read alignments from BAM files to fragment matrices (mxn)
- Construct haplotypes  in polyploid species

The haplotype assembly method uses a stepwise approach to reconstruct haplotypes. First haplotypes are reconstructed in a pre-set interval of snps. The haplotypes are reconstructed over a sliding window of 1. If these seed-haplotypes have a high quality they are extended. An extension is based on overlap between adjacent overlapping blocks. If multiple extensions are possible, the best extension is estimated. 


# Download 

1. Download the clone URL from the repository. 
2. Go to the location where you want the cloned directory to be made. 
3. There you go: It should work with python 2.7 and the most recent version of numpy. 

In addition bedtools, samtools, tabix and bgzip should be installed. 

# Process alignments and variants

The alignments should be present in a single-sample BAM file, and variants can be input as VCF files. 
Ideally a BED file of target intervals should be given as input, which will speed of the computation. 

Optimally the variants should only contain SNVs, as difficulties arise with parsing complex variation. 
The best strategy is to filter the variant before hand. 

At this moment the variants should have an **id**, for example SNP_01, or PotVarxxx.

The processing.py script will process the reads and variants, and score in which reads a SNP-allele occurs.
Do not remove homozygous variants which are heterozygous across many samples. These will be correctly processed in the haplotyping stage. 

```
python processing.py -i X.bam -vcf X.VCF -bed X.bed -o X.frg -tech PE;
```
Subsequently in X.frg all fragments are saved. If you want to see the fragment matrices you can put -save_matrics to True.

```
sort -k1V -k2n -k3n X.frg > X.sorted.frg
bgzip -f X.sorted.frg
tabix -s 1 -b 2 -e 3 X.sorted.frg.gz
```
# Perform haplotype assembly

The haplotype assembly can be performed using the following command. 
The default settings should apply for most cases. 
During the haplotyping a loose filtering is done on the SNPs. Therefore it might be that some SNPs are not haplotyped. 
In addition also the dosage estimation of variants is done during the haplotyping, so dosages of genotyping can vary from the dosages observed in a haplotype block. 

```
python local_haplotyping.py -i X.sorted.frg.gz -vcf X.vcf -bed x.bed -d -e -ploidy 4 -o X.bins
```

In the .bins files the reconstructed haplotype blocks are present.
The SNPs that are not processed into a haplotype are **not** present in these files. 

# Version history

2018 jun: Version 0.9.1: Bugfixes
2017 dec: Version 0.9: Most bugs are fixed. It works in diploids, tetraploids, hexaploids, but also with various sequencing data types such as pacbio/illumina or a combination of these. 




