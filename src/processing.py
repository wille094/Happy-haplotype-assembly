#!/home/johan/anaconda2/bin/python

"""
Created on Tue Feb 10 08:14:02 2015
@author: johan willemsen

"""

from __future__ import division
import numpy as np
import re
import os
import sys
import argparse
import datetime

import subprocess


class quality_filter:
    '''
    Class to interpret phred quality scores from sequencing data
    Also report the expected errors in read
    '''
    def __init__(self):
        self.quality_dict = {"!":0,'"':1,"#":2,"$":3,"%":4,"&":5,"'":6,"(":7,
                             ")":8,"*":9,"+":10,",":11,"-":12,".":13,"/":14,
                             "0":15,"1":16,"2":17,"3":18,"4":19,"5":20,"6":21,
                             "7":22,"8":23,"9":24,":":25,";":26,"<":27,"=":28,
                             ">":29,"?":30,"@":31,"A":32,"B":33,"C":34,"D":35,
                             "E":36,"F":37,"G":38,"H":39,"I":40,"J":41,"K":42, 
                             "L":43,"M":44,"N":45,"O":46,"P":47,"Q":48,"R":49,
                             "S":50,"T":51,"U":52,"V":53,"W":54,"X":55
                            }
    def lookup(self, ASCII):
        return ord(ASCII)-33

    def expected_errors_in_read(self,read):
        '''
        E = expected number of errors
        Is described in Edgar et al. (2015)
        With Q values of 30 you expect 1 error in 1000 bp?
        '''
        errors = [10**(-1*(self.lookup(i)/10)) for i in read]
        sum_errors = sum(errors)
        return int(sum_errors)

class log:
    '''
    Logging of errors and information (as otherwise it will be written to stdout)
    '''
    def __init__(self, log_name='proc.log'):
        self.log = open(log_name, 'a')
    def printl(self, *text):
        row = '\t'.join([str(i) for i in text])
        self.log.write(row + '\n')

class extract_interval:
    '''
    1. Read intervals from bed file and yield interval
    2. Read SAM file and return interval from BAM file
    3. Read VCF file and return interval from VCF file
    '''
    def __init__(self, BAM_file, VCF_file, BED_file):
        self.BAM_file = BAM_file
        self.VCF_file = VCF_file
        self.BED_file = BED_file

    def extract_sam(self, interval, bed=True):
        '''Extract SAM and save in variable'''
        if bed == True:
            # cmd = ['samtools', 'view', '-h', self.BAM_file]
            cmd = ['samtools', 'view', '-h', self.BAM_file, interval]
        else:
            cmd = ['samtools', 'view', '-h', self.BAM_file]

        process_SAM = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        
        read_information = process_SAM.stdout
        # print read_information
        return read_information

    def extract_vcf(self, interval):
        '''Extract VCF and save in variable'''

        g = open('interval_' + self.VCF_file.split('/')[-1] + '.bed', 'wb')
        g.write('\t'.join([str(j) for j in interval]) + '\n')
        g.close()

        cmd = ['bedtools', 'intersect', '-a', self.VCF_file, '-b', 'interval_' + self.VCF_file.split('/')[-1] + '.bed', '-header']
        process_VCF = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        
        snp_information = process_VCF.stdout
        
        return snp_information

    def get_bed_intervals(self):
        # read in bed intervals

        intervals_bed = []
        
        if self.BED_file != '-':
            interval_list = open(self.BED_file, 'r')
            
            # yield interval
            for line in interval_list:
                cnt = line.split('\t')
                interval1 = cnt[0] + ':' + cnt[1] + '-' + cnt[2]
                interval2 = [cnt[0], cnt[1], cnt[2].strip()]

                intervals_bed += [(interval1, interval2)]
        return intervals_bed

class processing():
    def __init__(self, fragments, variants, fragment_dict_file, matrix_file_name, mapq, baseq, expq, save=False, tech='PE'):
        self.SAM = fragments
        self.VCF = variants
        self.fragment_dict_file = open(fragment_dict_file, 'a')
        self.Q = quality_filter() #Use class quality scores. 
        self.mapQ = 13 
        self.baseQ = 13
        self.expQ = 100
        self.save = save
        self.tech = tech
        
        self.log = log()
        
        self.no = self.get_snp_positions()
        if self.no == True:
        
            self.parse_SAM_file()
            self.fragment_dict_file.close()
            #print save, self.save
            if self.save == True:
                #print 'save fragment'
                self.create_fragment_matrix()
                self.save_fragment_matrix(matrix_file_name)

    def get_snp_positions(self):
        '''
        Parse VCF files and report SNPs. 
        Returns True if snps are found and False if no snps are found. 
        '''
        VCF = self.VCF

        pattern = re.compile('^#')
        
        # SNP_table = []
        # for i in VCF:
        #     if not re.match(pattern,i):
        #         SNP_table += [i.strip().split('\t') ]
        
        SNP_table = [i.strip().split('\t') for i in VCF if not re.match(pattern,i)]

        if len(SNP_table) == 0:
            print 'No snps reported: aborting...'
            return False
        SNP_table = np.array(SNP_table, dtype='str')
        self.SNPlocation = np.array(SNP_table[:,1], dtype='int')
        self.SNPrefs = np.array(SNP_table[:,3], dtype='str')
        self.SNPalts = np.array(SNP_table[:,4], dtype='str')
        self.SNPids = np.array(SNP_table[:,2], dtype='str')
        self.SNPdos = np.array([i.split(':')[0].split('/').count('1') for i  in SNP_table[:,9]], dtype='str')
        self.chromosome = np.array(SNP_table[:,0], dtype='str')
        self.log.printl('number of snps :', len(self.SNPids))

        return True

    def SNP_type(self, ref, alt):
        '''
        Determines the type of snp and returns it
        M = Match
        S = soft-clipping
        D = Deletion
        I = Insertion
        '''
        if len(ref) == len(alt) and len(ref) > 1:
            SNP_type = 'M'
        elif len(ref) == len(alt) :
            SNP_type = 'S'
        elif len(ref) > len(alt):
            SNP_type = 'D'
        elif len(ref) < len(alt):
            SNP_type = 'I'
        return SNP_type

    def decode_cigar(self, cigar, start_read):
        '''
        Decode CIGAR string into whether a SNP occurs in a read or not.
        '''
        pattern = re.compile(r'([0-9]|[0-9][0-9])([A-Z])')
        a = pattern.findall(cigar)
        #print cigar
        #print a
        start = start_read
        b = [0, 'M']
        for item in a:
            if item[1] == 'M':
                start += int(item[0])
            elif item[1] == 'I':
                c = [start, 'I']
                b.append(c)
            elif item[1] == 'D':
                c = [start, 'D']
                b.append(c)
            else:
                start += int(item[0])
        #print b
        #return b

    def insertion_deletion(self, cigar, position_indel, start_read):
        '''
        Insertion and deletions are scored using decode cigar.
        '''
        cigar_list = self.decode_cigar(cigar, start_read)
        allele = 0  # 0
        for item in cigar_list:
            if (cigar_list[0] > position_indel - 2) and (cigar_list[0] < position_indel + 2):
                allele = 1  # 1
            else:
                allele = 0  # 0
        return allele

    def offset(self, read_cigar):
        offset_list = []   

        for l, m in read_cigar:
            #print m
            if m == 'M':
                offset_list += [0] * int(l)
            elif m == 'I':
                offset_list += [-1]*int(l)
            elif m == 'D':
                offset_list += [0]*int(l)
            elif m == 'S':
                offset_list += [0] *int(l)
        return offset_list

    def pseudo_alignment_read(self, read_cigar, read, qual):
        '''  Does pseudo alignment of read with reference genome, using the CIGAR code clipped for clipped bases
        '''
        total_insertions = 0
        start = 0
        align = ''
        qual_align = ''
        #print read_cigar
        for l, m in read_cigar: 
            if m == 'M':
                align += read[start:start + int(l)]
                qual_align += qual[start:start + int(l)]

                start = start + int(l)
            elif m == 'I':
                align += read[start:start+int(l)]
                qual_align += qual[start:start+int(l)]

                start = start + int(l)
                total_insertions += int(l)
            elif m == 'D':
                align += int(l)*'-'
                qual_align += int(l)*'-'

                pass
            elif m == 'S':
                align += read[start:start + int(l)]
                qual_align += qual[start:start + int(l)]
                start = start + int(l)
        return align, qual_align, total_insertions

    def get_adjusted_position(self, align, qual_align, pos, length_snp):
        off = 0
        #print len(align)
        for index, i in enumerate(align): 
            off += self.offset_list[index]
            #print index, index + off, pos
            if index + off == pos:
                #print 'SNP at adjusted index', pos, '>', index, index + length_snp

                return align[index:index+length_snp], qual_align[index:index+length_snp], index
                break

    def get_allele(self, read_start, read_end, ref, alt, pos, type_SNP, seq, cigar, qual):
        '''
        Determines whether SNP gets zero or one as value per read.
        '''
        allele = 'NA'  # basis allele 3
        #print 'startsgetallel'
        if pos > read_start and pos < read_end: #read does have snp in there, but soft clipping is not removed  in  read.  
            unadj_start_snp_in_read = pos - read_start 
            # CHECK offset and get allele at position
            # print unadj_start_snp_in_read, ref, alt, type_SNP
            pattern = re.compile(r'([0-9]|[0-9][0-9])([A-Z])')
            pattern = re.compile(r'([0-9]*)([A-Z])')

            read_cigar = pattern.findall(cigar)
            #print  read_cigar
            #print len(seq)

            if 'S' in read_cigar[0]:
                seq = seq[int(read_cigar[0][0]):]
                qual = qual[int(read_cigar[0][0]):]
                read_cigar.pop(0)

            if 'S' in read_cigar[-1]:
                seq = seq[:-int(read_cigar[-1][0])]
                qual = qual[:-int(read_cigar[-1][0])]
                read_cigar.pop(-1)

            # After cigar code processing (chopping of S bases), a offset list is made. 

            self.offset_list = self.offset(read_cigar)
            #print self.offset_list

            align, qual_align, insertions = self.pseudo_alignment_read(read_cigar,seq, qual)
            #print 'align', align
            assert  len(align) == len(self.offset_list)
            assert len(align) == len(qual_align)

            #Note soft-clipping causes an adjusted read start.  
            # 1 bp adjusted

            start = read_start + len(align)

            if (len(align) - align.count('-') + sum(self.offset_list)) < unadj_start_snp_in_read+1:
                return '-', '-'

            if unadj_start_snp_in_read >= len(align) - 1:
                return '-', '-'

            base, base_quality, ind = self.get_adjusted_position(align, qual_align, unadj_start_snp_in_read, max(len(alt), len(ref)))


            if type_SNP == 'S':
                if  base == ref:
                    allele = 0
                else:
                    allele = 1
            elif type_SNP == 'I':

                #print 'ind', ind
                try:
                    next = self.offset_list[ind+1] #TODO: Problem GIVES A PROBLEM gives a problem
                except:
                    next = 'u' #print ind, len(self.offset_list), len(seq)
                if next == -1:
                    allele = 1
                elif next == 'u':
                    allele = '-'
                else:
                    allele = 0

                base_quality = 'NA'

            elif type_SNP == 'D':
                #print align[ind:ind+1]
                if '-' in base:
                    allele = 1
                else:
                    allele = 0
                base_quality = 'NA'
            elif type_SNP == 'M':
                if base == ref:
                    allele = 0
                else:
                    allele = 1

            #print 'allele : ', allele
        return allele, base_quality

    def parse_SAM_file(self):
        '''
        Function to read SAM file (preferable a chunk of a sam file to decrease computation time)
        '''
        
        SAM = self.SAM

        #Get unique fragment identifiers
        fragment_set = set()
        fragment_dict = {}
        quality_dict = {}
        pattern = re.compile('^\@')

        single_end_fragments = []
        cnt = 0 
        for f in SAM:
            try: # General execption, if a read fails, just continue!
                # print f
                match = re.match(pattern,f)

                read = f.split('\t')
                read_name = read[0]

                if match:
                    self.log.printl(read_name, 'not used')
                    continue
                else:
                    self.log.printl(read_name, 'used')


                fragment_set.add(read_name)
                flag = int(read[1])
                position = int(read[3])
                CIGAR = read[5]
                seq = read[9]
                length_read = len(seq)
                mapping_quality = int(read[4])
                read_quality = read[10]

                indices_snps = np.where((self.SNPlocation > position) & (self.SNPlocation < position + length_read))[0]

                self.log.printl(read_name, len(indices_snps))

                # Determine if any of both reads is included in the file. 
                cnt +=1

                #print read_name
                if read_name not in fragment_dict:
                    fragment_dict[read_name] = {}  
                    quality_dict[read_name] = {}              
                    single_end_fragments += [read_name]
                    PE = False 
                elif self.tech == 'PE':
                    PE = True
                    single_end_fragments.remove(read_name)
                #elif self.tech == 'SE':
                #    continue # should not be allowed, because reads are duplicated then.

                # for each fragment that has more than one snp, calculate the allele in question. 

                if len(indices_snps) > 0:

                    for index in indices_snps:
                        type_SNP = self.SNP_type(self.SNPrefs[index], self.SNPalts[index])
                        #print type_SNP,
                        allele, base_quality = self.get_allele(position, position + length_read, self.SNPrefs[index], self.SNPalts[index], self.SNPlocation[index], type_SNP, seq, CIGAR, read_quality)
                        #print allele
                        #print self.SNPrefs[index], self.SNPalts[index], type_SNP, index, allele
                        fragment_dict[read_name][index] = (allele, base_quality) # Here I should change the index to name of the snps
                        #quality_dict[read_name][index] = base_quality
                    
                #  Save chunks of reads to the fragment_dict file (make sure that the reverse end is included)
                    
                if  PE == True or self.tech == 'SE':
                    if self.save == False:
                        read = fragment_dict.pop(read_name)
                        if read != {}:
                            self.save_fragment_read(read)    
                    else:
                        if fragment_dict[read_name] != {}:
                            self.save_fragment_read(fragment_dict[read_name])
                

                # Determine which level the read should be considerered as finished. 

                if PE == False and self.tech == 'PE':
                    if len(single_end_fragments) > 50000:
                        single_read = single_end_fragments[0] # remove the oldest read
                        single_end_fragments.remove(single_read)
                        read = fragment_dict.pop(single_read)
                        
                        if read != {}:
                            self.save_fragment_read(read)

            except:
                pass

            self.log.printl('number of fragments used', cnt)
            # Save rest of single end fragments!

        if fragment_dict != {}:
            for read in fragment_dict:
                if fragment_dict[read] != {}:
                    self.save_fragment_read(fragment_dict[read])
        self.fragment_dict = fragment_dict

    def save_fragment_read(self, read, index='names'):
        '''
        Creates a fragment dictionary file with chr, start, end as columns
        '''
        # k = open(self.fragment_dict_file, 'a')
        #self.fragment_dict = sorted(self.fragment_dict.values(), key = lambda y: sorted(y)[0])

        #self.log.printl(read)

        start_snp = min(read.keys())
        end_snp = max(read.keys())
        location_start = self.SNPlocation[start_snp]
        location_end = self.SNPlocation[end_snp]
        chromosome = self.chromosome[start_snp]
        if index == 'numeric':
            entry = chromosome + '\t' + str(location_start) + '\t' + str(location_end) + '\t' + str(read)
        else:
            adjusted_read = {}
            for i in read:
                adjusted_read[self.SNPids[i]] = read[i]

            entry = chromosome + '\t' + str(location_start) + '\t' + str(location_end) + '\t' + str(adjusted_read)
        
        self.fragment_dict_file.write(entry +'\n')

    def create_fragment_matrix(self):
        '''
        Compute fragment matrix from set of variants and reads.

        Args:
            variants: list of variants and dosages
            read_set: list of dictionaries with read information (SNP:allele)
            name_file: output file in which the fragment matrix should be saved
            save: True if fragment matrix should be saved, False if not.

        Returns:
            fragment_matrix: nxm matrix where n=snps and m=reads

        '''

        k = len(self.SNPlocation)
        h = len(self.fragment_dict)

        fragment_matrix = np.zeros((h, k), dtype="S2")
        fragment_matrix.fill('-')
        print self.fragment_dict.values()[0].keys()

        # self.fragment_dict = sorted(self.fragment_dict.values(), key = lambda y: (sorted(y.keys()) + [0])[0])

        if h == 0:
            print 'No accurate fragments observed'
            self.fragment_matrix = np.zeros((0,0))
        else:
            
            # for index, read in enumerate(self.fragment_dict):
            #     #index is index, read is key
            #     for j in read:
            #         fragment_matrix[index][j] = read[j]
            self.read_names = []
            
            for read_index, read in enumerate(self.fragment_dict):
                #print index
                self.read_names += [read]
                for j in self.fragment_dict[read]:
                    fragment_matrix[read_index][j] = self.fragment_dict[read][j][0] # add base, not the qual
            self.fragment_matrix = fragment_matrix
    def save_fragment_matrix(self, name_file):
        
        head1 = '\t'.join(self.SNPids)
        head2 = '\n' + '\t'.join(self.SNPdos)
        head3 = '\n' + '\t'.join([str(i) for i in self.SNPlocation])
        if self.fragment_matrix.shape != (0,0):
            heads = head1+head2+head3
            foots = '\n '.join(self.read_names)
            np.savetxt(name_file, self.fragment_matrix, fmt="%s", header=heads, footer=foots)

if __name__ == "__main__":
    
    description_text = 'Processing of SAM files into fragment matrices.'
    epilog_text = 'Copyright Johan Willemsen from may 2014-end-of-time'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    parser.add_argument('-i', type=str,help='Input BAM file, if - then read from stdin')
    parser.add_argument('-vcf', type=str, help='Input VCF file')
    parser.add_argument('-bed', type=str, default='-', help='Input BED file')
    parser.add_argument('-o', type=str, default='-', help='Output fragment file')

    parser.add_argument('-mapq', type=int, default=13, help='Minimal mapping quality (phred) for input reads. The default value is 13')
    parser.add_argument('-expq', type=int, default=2, help='Minimal expected errors for input reads. The default is 2')
    parser.add_argument('-baseq', type=int, default=13, help='Minimal base quality for variant positions (note only applicable to snps/mnps/insertions. The default is 13')
    
    parser.add_argument('-save_matrices', type=str, default='True', help='save_matrix?')
    parser.add_argument('-matrix_prefix', type=str, default='temp', help='Matrix prefix' )
    parser.add_argument('-tech', type=str, default='PE', help='tech type' )

    args = parser.parse_args()

    if args.i == '-':
        BAM_file = sys.stdin
    else:
        BAM_file = args.i
    
    if args.o == '-':
        fragment_dict_file = sys.stdout
    else:
        fragment_dict_file = args.o

    if args.save_matrices == 'True':
        save_matrices = True
    else:
        save_matrices = False
    #print save_matrices

    a = extract_interval(BAM_file, args.vcf, args.bed)
    bed_intervals = a.get_bed_intervals()
    if bed_intervals != []:
        for (interval, interval2) in bed_intervals:
            
            logs = log()
            logs.printl(interval.strip())
            snp_information = a.extract_vcf(interval2)
            read_information = a.extract_sam(interval, bed=True)
            matrix_file_name = args.matrix_prefix + '_' +  '_'.join(interval2) + '.frg'

            b = processing(read_information, snp_information, fragment_dict_file,  matrix_file_name, args.mapq, args.baseq, args.expq, save=save_matrices, tech=args.tech)
    else:
        #print 'No bed file reported, so use the whole VCF filename this only works if the reads originate from one scaffold/chr!'

        snp_information = open(args.vcf,'r')
        read_information = a.extract_sam(None, bed=False)
        matrix_file_name = args.matrix_prefix + '.frg'

        b = processing(read_information, snp_information, fragment_dict_file,  matrix_file_name, args.mapq, args.baseq, args.expq, save=save_matrices)

