#!/home/johan/anaconda2/bin/python
from __future__ import division
import random, math
import itertools
import numpy as np
import copy
import time
import sys
from collections import Counter
import argparse
import subprocess
import re


class extract_interval:
    '''
    1. Read intervals from bed file and yield interval
    2. Read SAM file and return interval from BAM file
    3. Read VCF file and return interval from VCF file
    '''
    def __init__(self, FRG_file, VCF_file, BED_file):
        self.FRG_file = FRG_file
        self.VCF_file = VCF_file
        self.BED_file = BED_file

    def extract_frg(self, interval, bed=True):
        '''Extract fragments from file, and save in variable'''
        if bed == True:
            cmd = ['tabix', '-f', self.FRG_file, interval]
        else:
            cmd = ['tabix', '-f', self.FRG_file, '.']

        process_FRG = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        frg_information = process_FRG.stdout.readlines()
        process_FRG.terminate()

        return frg_information

    def extract_vcf(self, interval, bed=True):
        '''Extract VCF and save in variable'''
        if bed == True:
            g = open('interval_' + self.VCF_file.split('/')[-1] + '.bed', 'wb')
            g.write('\t'.join([str(j) for j in interval]) + '\n')
            g.close()

            cmd = ['bedtools', 'intersect', '-a', self.VCF_file, '-b', 'interval_' + self.VCF_file.split('/')[-1] + '.bed', '-header']
            process_VCF = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            snp_information = process_VCF.stdout.readlines()
            process_VCF.terminate()

        else:
            k = open(self.VCF_file, 'r')
            snp_information = k.readlines()

        return snp_information

    def get_bed_intervals(self):
        # read in bed intervals

        intervals_bed = []
        
        if self.BED_file != '-':
            interval_list = open(self.BED_file, 'r')
            
            # yield interval
            for line in interval_list:
                cnt = line.split('\t')
                interval1 = cnt[0] + ':' + cnt[1] + '-' + cnt[2]
                interval2 = [cnt[0], cnt[1], cnt[2]]

                intervals_bed += [(interval1, interval2)]
        return intervals_bed

def write_bins(output_file, temp_hap, temp_names, index_block):
    '''
    Writes the bins to output_file
    '''
    if output_file == sys.stdout:
        g = sys.stdout
    else:
        g = open(output_file, 'a')

    g.write('block_' + str(index_block) +'\t\n' )
    
    if temp_hap[0] == 0 or temp_hap[0] == 1:
        hap_row = temp_names[0] + '\t' + '\t'.join([str(m) for m in temp_hap]) + '\n'
        g.write(hap_row)

    else:
        hap_rows = np.array(temp_hap).T.tolist()
        for index, i in enumerate(hap_rows):
            # print index, i
            hap_row = temp_names[index] + '\t' + '\t'.join([str(m) for m in hap_rows[index]]) + '\n'
            g.write(hap_row)
    g.write('\n')
    g.close()

def write_haplotype_prob(output_file, bins, names, variants):

    if output_file == sys.stdout:
        g = sys.stdout
    else:
        g = open(output_file, 'a')

    indices = sorted(bins.keys())
    # print indices
    for index in indices:
        block = str(index) + '\t' + names[index] + '\t' + '\t' + '\t'.join([str(i) for i in bins[index]]) + '\n'
        g.write(block)

    g.close()

def write_haplotype_bins(output_file, bins, names, odds=False):
    if output_file == sys.stdout:
        g = sys.stdout
    else:
        g = open(output_file, 'a')
    
    indices = sorted(bins.keys())
    for index_block in indices:
        if odds == False:
            g.write('block_' + str(index_block) +'\t' + '\n')
        else:
            g.write('block_' + str(index_block) +'\t' + odds[index_block] +  '\n')

        hap_rows = np.array(bins[index_block]).T.tolist()
        for index, i in enumerate(hap_rows):
            # print index, i
            hap_row = names[index] + '\t' + '\t'.join([str(m) for m in hap_rows[index]]) + '\n'
            g.write(hap_row)
        g.write('\n')
    g.close()

def write_reads(output_file, bins_posteriors, fragment_names):
    if output_file == sys.stdout:
        g = sys.stdout
    else:
        g = open(output_file, 'a')

    indices = sorted(bins_posteriors.keys())

    for index_block in indices:
        # Dictionary mapping read id to posterior. 
        g.write('\nindexbolcks %i\n' % (index_block))
        # print 'haps', list(set(bins_posteriors[index_block].values()))
        reads_dict = bins_posteriors[index_block]
        haps = list(set(bins_posteriors[index_block].values()))
        for i in haps:
            reads = ''
            for j in reads_dict:
                if reads_dict[j] == i:
                    reads += fragment_names[j] + '\n'
            if reads != '': 
                g.write('hap %i\n' % (i))
                g.write(reads)
    g.close()


class read_data2:
    '''
    Reads the fragment matrix. 
    '''
    def __init__(self, snp_information, read_information, ploidy, no_reads = 2, per_reads=0.1):

        self.snp_information = snp_information
        self.read_information = read_information
        self.no_reads = no_reads
        self.per_reads = 0.1
        self.ploidy = ploidy
        self.read_snp_information() # Create SNP_table
        
        self.create_fragment_dictionary()
        self.create_fragment_matrix()
        self.SNP_selection()

    def create_fragment_dictionary(self):
        self.fragments = []
        #self.snps = []
        self.read_names = []
        for index, fragment in enumerate(self.read_information):
            content_frag = fragment.split('\t')
            read_dict = eval(content_frag[3])
            self.read_names += ['read%i' %(index)]
            
            # adjusted version for if the names of snps are used. 

            # Here an error occurs if the window is homozygous, assumptiojn.  
            adjusted_read_dict = {}
            for i in read_dict:
                if i in self.names:
                    index_snp = self.names.tolist().index(i)
                    adjusted_read_dict[index_snp] = read_dict[i]

            if adjusted_read_dict != {}:
                self.fragments += [adjusted_read_dict]


    def create_fragment_matrix(self):
        '''
        Compute fragment matrix from a fragment dict. 
        fragment_matrix: nxm matrix where n=snps and m=reads
        '''
        self.k = len(self.snps)
        self.h = len(self.fragments)

        fragment_matrix = np.zeros((self.h, self.k), dtype="S2")
        fragment_matrix.fill('-')

        #print self.k, self.h

        if self.h == 0:
            # print 'No accurate fragments observed'
            self.fragment_matrix = np.zeros((0,0))
        else:
            
            for read_index, read in enumerate(self.fragments):
                #print read_index, len(read), read
                for j in read:
                    try:
                        fragment_matrix[read_index][j] = read[j][0]
                    except:
                        pass

        self.fragment_matrix = fragment_matrix        
        #np.savetxt('test.frag', self.fragment_matrix, fmt="%s")

    def SNP_selection(self):
        '''
        SNP filter based on read depth. 
        If a SNP has no reads, then show it as uncalled ....
        If a SNP has less than 0.1 of the reads alternative or reference, then consider it homozygous. 

        selection of snps based on parsed reads. Allows inputting all snps in population, instead of just the heterozygous snps. 
        These snps shoule be remembered and inserted in the bins file. 
        '''

        # read in fragment matrix over columns (snps) and make two list of zeros

        count_ref = []
        count_alt = []

        #print self.fragment_matrix
        for column_index, column in enumerate(self.fragment_matrix.T):
            # print self.names[column_index], column.tolist().count('1'), column.tolist().count('0')

            c = column.tolist()
            ref = c.count('0')
            alt = c.count('1')

            count_ref += [ref]
            count_alt += [alt]

        # Count which snps seem not realistic to be heterozygous. 
        homozygous_snps = [] 
        bins_homozygous = []
        
        for index, r in enumerate(count_ref):
            a = count_alt[index]
            # print index, a, r, a+r, self.names[index]
            if a+r == 0:
                homozygous_snps += [index]
                bins_homozygous += [['.']*self.ploidy]
                continue

            frac = min([a / (a + r), r / (a+r)])

            if (r >= self.no_reads) and ( a >= self.no_reads ) and (frac > self.per_reads):
                pass
            else:
                #print 'homozygous snp observed'
                homozygous_snps += [index]

                dos_alt = a / (a + r)
                dos_ref = r / (a + r)

                if dos_alt > dos_ref:
                    bins_homozygous += [[1]*self.ploidy]
                elif dos_ref > dos_alt:
                    bins_homozygous += [[0]*self.ploidy]
                else:
                    bins_homozygous += [['.']*self.ploidy]



        # There can be many more snps in the fragment file than in the snp file. Hence either I need to change numbering to chr_loc later.
        # Delete the homozygous snps from the fragment file (can later be inserted again)

        self.deleted_snps = [i for index, i in enumerate(self.snps) if index  in homozygous_snps] # get dosage of snps in play
        self.initial_homozygous_snps_indices = homozygous_snps
        self.initial_het_snps_indices = [i for i in range(self.fragment_matrix.shape[1]) if i not in homozygous_snps]
        self.bins_homozygous = bins_homozygous
        self.homo_names = self.names[self.initial_homozygous_snps_indices]


        # save the all snps vector and the all_snps_names. 
        self.all_snps = self.snps[:]
        self.all_snps_id = self.names[:]

        # adjust the fragment matrix and dosage vector
        self.fragment_matrix = np.delete(self.fragment_matrix, homozygous_snps, axis=1)
        self.snps = [i for index, i in enumerate(self.snps) if index not in homozygous_snps]

        self.h = self.fragment_matrix.shape[0]
        self.k = self.fragment_matrix.shape[1]

        # Delete the homozygous snps from the snp_information (otherwise it is saved in the bins file)
        self.location = self.location[self.snps]
        self.SNPrefs = self.SNPrefs[self.snps]
        self.SNPalts = self.SNPalts [self.snps]
        self.names = self.names[self.snps]
        self.variants = self.variants[self.snps]
        self.chromosome = self.chromosome[self.snps]

        # save the snps
        abc = open('%s.bed' % (args.o.rstrip('.bins')), 'a')

        for index, i in enumerate(self.chromosome):
            snp = [i, str(self.location[index]-1), str(self.location[index]), str(self.names[index]) + str(self.SNPrefs[index]) + str(self.SNPalts[index])]
            abc.write('\t'.join(snp) + '\n')
        abc.close()
        
    def read_snp_information(self):
        '''
        Parse VCF files and report SNPs. 
        Returns True if snps are found and False if no snps are found. 
        '''
        VCF = self.snp_information

        pattern = re.compile('^#')
        SNP_table = [i.strip().split('\t') for i in VCF if not re.match(pattern,i)]
        # print len(SNP_table)

        if len(SNP_table) == 0:
            # print 'No snps reported: aborting...'
            return False
        SNP_table = np.array(SNP_table, dtype='str')


        self.location = np.array(SNP_table[:,1], dtype='int')
        self.SNPrefs = np.array(SNP_table[:,3], dtype='str')
        self.SNPalts = np.array(SNP_table[:,4], dtype='str')
        self.names = np.array(SNP_table[:,2], dtype='str')
        self.variants = np.array([i.split(':')[0].split('/').count('1') for i  in SNP_table[:,9]], dtype='str')
        self.chromosome = np.array(SNP_table[:,0], dtype='str')

        self.snps = range(len(self.location))

class read_data:
    '''
    Reads the fragment matrix. 
    '''
    def __init__(self, input_file):

        if input_file != sys.stdin:
            matrix = open(input_file)
        else:
            matrix = sys.stdin

        self.names = matrix.next().strip().lstrip('# ').split('\t')
        self.variants = [int(i) for i in matrix.next().strip().lstrip('# ').split('\t')]
        self.location = [int(i) for i in matrix.next().strip().lstrip('# ').split('\t')]
        try: 
            self.fragment_matrix = np.loadtxt(matrix, dtype='str')
        except IOError: 
            self.fragment_matrix = np.array([])


        self.fragment_names = []

        if input_file != sys.stdin:
            matrix = open(input_file)
        else:
            matrix = sys.stdin

        for i in matrix:
            if 'HWI' in i:
                self.fragment_names += [i.strip('# ').strip()]
        #print 'number of fragments', len(self.fragment_names)
        try:        
            self.k = self.fragment_matrix.shape[1]
        except:
            if self.fragment_matrix.tolist() == []:
                self.k = 0
            else:
                self.k=1
    def read_bin_file(self,bins_file):
        #read bins file
        pass

class helper_functions():
    
    def phred(self, x):
        '''Calculated the p-value from PHRED like scores (base quality)'''
        p = 10 ** (-1 * (x / 10))
        return p

    def unique_rows(self, data):
        arr = np.array(data)        
        uniq = np.unique(arr.view(arr.dtype.descr * arr.shape[1]))
        return len(uniq.view(arr.dtype).reshape(-1, arr.shape[1]))    

    def create_all_possible_genotypes(self, dosages, interval_length):
        d = dosages
        i = 0
        while i < interval_length - 1:
            #print i
            temp = []
            for k in d:
                for j in dosages:
                    # print k, j
                    temp += [str(k) + str(j)]
            i += 1
            # print temp
            d = temp

        for index, i in enumerate(d):
            d[index] = list([int(element) for element in i])
            #d[index] = list([str(element) for element in i]) # temporary fix for fixing haplotype strings

        return d

    def calculate_all_phasings(self, haps, ploidy):
        phasings_full = list(itertools.combinations_with_replacement(haps, ploidy))
        return phasings_full

    def calculate_phasing_configurations_constrained_on_g(self, haps, ploidy, genotype):
        temp = []
        temp_indices = []
        for n in itertools.combinations_with_replacement(haps, ploidy):
            # print n
            sums = np.array(n).sum(axis=0)
            # print genotype, sums
            minus = sums - np.array(genotype)
            if np.count_nonzero(minus) == 0:
                temp += [n]
                temp_indices += [[haps.index(i) for i in n]]

        return temp, temp_indices

    def create_reduced_list_for_genotypes(self, all_G, all_H, ploidy=4):
        all_P_genotypes = {}
        all_P_genotypes_indices = {}
        for index, g in enumerate(all_G):
            #print index
            red_P, red_P_indices = self.calculate_phasing_configurations_constrained_on_g(all_H, ploidy, g)
            # print red_P
            all_P_genotypes[index] = red_P
            all_P_genotypes_indices[index] = red_P_indices
        
        
        str_genotypes = []
        for i in all_G:
            str_temp = ''
            for j in i:
                str_temp += str(j)

            str_genotypes += [str_temp]
        return all_P_genotypes, all_P_genotypes_indices

    def select_reads_per_bin(self, index, k, fragment_matrix, ungapped = True, large=False):
        
        '''Select reads in particular bin'''
        start = index
        if large is False:
            end = start + k
        else:
            end = large
        interval = end - start

        bin_array = []
        bin_array_read_indices = []

        for index,read in enumerate(fragment_matrix[:, start:end]):
            count = read.tolist().count('-')
            #print count,
            if ungapped == True:
                if count == 0:
                    bin_array += [read.tolist()]
                    bin_array_read_indices += [index]
            if ungapped == False:
                #print read
                if count <= interval -2:
                    #print read            #only for 4 snp?
                    bin_array += [read.tolist()]
                    bin_array_read_indices += [index]

        return bin_array, bin_array_read_indices

    def get_number_of_correct_haps(self, haps, selection):
        score = 0
        sel = selection[:]
        for hap in haps:
            if hap in sel:
                score += 1
                sel.pop(sel.index(hap))
            else:
                pass

        return score

class likelihood_calc():
    
    def get_probabilities_haplotype(self, fi, haplotype, quality):
        '''
        Calculates probability using PHRED if haplotype and read match
        fi = read X
        '''
        pr = 1.0
        for index, j in enumerate(fi):
            if j != str(haplotype[index]) and j != '-':
                #pr *= 0.01 #self.phred(quality[index])
                pr *= quality[index]
            elif j == str(haplotype[index]):
                #pr *= (1 - self.phred(quality[index]))
                pr *= (1 - quality[index])

        return pr

    def get_likelihood_naive(self, reads, haps, qualities):
        '''
        Get likelihood for each phasing given a set of input reads and possible haplotypes
        - per read-haplotype combination multiply
        - sum across solutions

        Returns an array with probilities where each read belongs. 

        '''
        hap_prob_array = np.zeros((len(reads), len(haps)), dtype='float64')
        for index, hap in enumerate(haps):                               
            hap_prob = []
            for i, fi in enumerate(reads):
                hap_prob += [self.get_probabilities_haplotype(fi, hap, qualities[i])]
            hap_prob_array[:,index] = np.array(hap_prob, dtype='float64')
        return hap_prob_array

    def estimate_posteriors_individual_reads(self, hap_prob_array, read_array_indices, selection=None):
        ''' 
        Each read has probability p matching a haplotype, which is based on base calling probabilities. 
        The maximum probability is the read which has the best matchings. If a read is ungapped it should produce one index

        If one particular phasing solution is provided (indices of haps) then the maximum probability 
        within this phasing are choosen. 

        returns: Dictionary mapping read id to posterior. 

        '''
        posterior = {}
        
        #test = {}

        if selection == None:
            for read_index, read in enumerate(hap_prob_array):
                indices = np.argwhere(read == np.amax(read))
                # print read, indices
                if len(indices) == 1:
                    posterior[read_index] = int(indices[0])
        else:
            new_select = list(set(selection)) #non redundant selection
            select = hap_prob_array[:,new_select] 
            #print select.shape
            for read_index, read in enumerate(select):
                indices = np.argwhere(read == np.amax(read))
                #print read_index, indices
                if len(indices) == 1:
                    posterior[read_index] = new_select[int(indices[0])]
               
        adjusted_posterior = {}
        for index, read_index in enumerate(read_array_indices):
            if index in posterior:
                adjusted_posterior[read_index] = posterior[index]

        return posterior

    def get_likelihood_for_all_solutions_naive(self, hap_prob_array, phasings):
        '''
        calculate the Likelihoods for all solutions for all phasings. 
        sum over four haplotypes, then log then sum of logs is equivalent to product of prob
        '''

        LHs = np.array([hap_prob_array[:,haps].sum(axis=1) for haps in phasings], dtype=np.longdouble)
        all_lm = np.log(LHs).sum(axis=1)

        return all_lm

    def calculate_maximum_likelihood(self, all_lm):
        m = all_lm.max()
        total = np.log(np.sum(np.exp(all_lm - m))) + m

        # likelihood ratio based on two haplotype solutions
        # P(MLE) / P(MLE2) = np.exp(MLE - MLE2)
        
        MLE = all_lm.max()
        indices = np.where(all_lm == all_lm.max())[0]
        
        if len(indices) == 1:
            selection = int(indices)    
            idxs = np.delete(all_lm, selection, axis=0)
            MLE2 = idxs.max()
            ratio = np.exp(MLE - MLE2)
        else:
            ratio = 1
            selection = int(indices[0])

        #check if ratio is not strange
        if ratio <= 0 or np.isfinite(ratio) == False:
            # print '##'
            # print 'odds', odds
            # print 'all_lm', all_lm
            # print 'selection', selection
            # print 'MLE', MLE
            pass
        return selection, MLE, ratio

class ML_haplotyping(helper_functions, likelihood_calc):
    
    def __init__(self, interval_length=4,  dosage_estimation=False, ploidy=4, cnt=True):
        '''
        Here the haplotyping is initiated. 
        All haplotypes are considered given {interval_length} and {ploidy}. 

        2**n haplotypes --> 

        Given the dosages, we could put a prior on the genotypes, or even limit the number of solutions by the dosages. 

        '''
        self.k = interval_length # Normally I use k for ploidy, here I did make an mistake to code it a k. 
        self.cnt = cnt
        het_snps = range(0,ploidy+1)
        self.all_G = self.create_all_possible_genotypes(het_snps, self.k)
        self.all_H = list(self.create_all_possible_genotypes(['0', '1'], self.k))
        self.all_P = self.calculate_all_phasings(self.all_H, ploidy)
        
        # flexible seeds

        self.all_H_dict = {}
        self.all_P_dict = {}
        self.all_P_indices_dict = {}

        for i in range(1,self.k+1):
            self.all_H_dict[i] = list(self.create_all_possible_genotypes(['0', '1'], i))
            self.all_P_dict[i] = self.calculate_all_phasings(self.all_H_dict[i], ploidy)
            self.all_P_indices_dict[i] = list(itertools.combinations_with_replacement(range(len(self.all_H_dict[i])), ploidy))

        # Non-flexible seeds

        if dosage_estimation == False:
            self.all_P_genotypes, self.all_P_genotypes_indices = self.create_reduced_list_for_genotypes(self.all_G, self.all_H, ploidy)
            # print len(self.all_P_genotypes), len(self.all_P_genotypes_indices)
        else:
            self.all_P_indices =  list(itertools.combinations_with_replacement(range(len(self.all_H)), ploidy))

        self.dosage_estimation = dosage_estimation

    def one_block_binning(self, i, hap_length, fragment_matrix, ungapped_reads=False):
        ''' Make a haplotype using full likelihood for a single block '''

        read_array, read_array_indices = self.select_reads_per_bin(i, hap_length, fragment_matrix, ungapped=ungapped_reads)
        qualities = np.zeros((len(read_array), hap_length))      #Need to add phred score computation here. 
        qualities.fill(0.001)

        all_hap = self.all_H_dict[hap_length]
        all_P_selection = self.all_P_indices_dict[hap_length]
        possible_phasings =  self.all_P_dict[hap_length]

        hap_prob_array = self.get_likelihood_naive(read_array, all_hap, qualities)                # compute haplotype probabilities
        all_lm = self.get_likelihood_for_all_solutions_naive(hap_prob_array, all_P_selection)     # compute all likelihoods
        selection_phasing, MLE, ratio = self.calculate_maximum_likelihood(all_lm)                 # compute maximum likelihood, returns the ith phasing number
            
        solution = possible_phasings[selection_phasing]
        posterior = self.estimate_posteriors_individual_reads(hap_prob_array, read_array_indices, selection=all_P_selection[selection_phasing])

        return solution, posterior, MLE, ratio, read_array

    def iterative_binning(self, variants, fragment_matrix, LR_ratio_thres, snp_location, overlap=1, ungapped_reads=False):
        '''
        Iterative binning over snp interval. 
        The maximmum snp-set is set with self.k

        If that interval does not return a correct haplotype, the interval is made smaller. 
        This recovers more snps, than with using a single pre-set interval. 
        '''
        bins = {}
        oddslist = {}
        bin_array_list = {}
        posterior_dict = {}

        for i in range(0,len(variants) -1, overlap):

            succes = False
            seed_length = self.k

            # If the seed is not successfull, lower the interval size, hence flexible seeds. 

            while succes == False and seed_length > 1: 
                solution, posterior, MLE, ratio, read_array = self.one_block_binning(i, seed_length, fragment_matrix, ungapped_reads=False)
                
                if ratio > LR_ratio_thres:
                    succes = True
                    seed_length = 1
                else:
                    seed_length -= 1

            bins[i] = solution
            oddslist[i] = ratio
            bin_array_list[i] = read_array
            posterior_dict[i] = posterior
        return bins, oddslist, bin_array_list, posterior_dict

class local_haplotyping_continous(helper_functions, likelihood_calc):
    def __init__(self, interval_length = 4, ploidy=4):
        '''
        Initialization of local haplotyping algorithm. Estimates the number of haplotypes needed and possible phasings.         
        This option would work in on a bulked segrant analysis. As input we can use the bins file, with estimated haplotypes
        For each haplotype then the frequency would be estimated in both resistant as susceptible parent. 

        '''
        self.k = interval_length
        self.all_H = list(self.create_all_possible_genotypes([0, 1], self.k))

    def get_hap_continous(self, reads, qualities, read_array_indices, count=False):
        '''
        First calculate the probabilities for all 24 haplotypes. 
        Each haplotype gets a score, based on the reads. 

        We can count this (estiamted posterior)
        or just output a probabilistic estimate for each haplotype.

        '''
        hap_prob_array = self.get_likelihood_naive(reads, self.all_H, qualities)
        
        if count == True:
            posterior = self.estimate_posteriors_individual_reads(hap_prob_array, read_array_indices, selection=None) # assigns reads to haplotypes
            detected_haplotypes = set(posterior.values())
            results_haps = np.array([posterior.values().count(i) for i in range(len(self.all_H))])
        else:
            # print hap_prob_array.sum(axis=0)
            results_haps = hap_prob_array.sum(axis=0) / hap_prob_array.sum(axis=0).sum()
            posterior = self.estimate_posteriors_individual_reads(hap_prob_array, read_array_indices, selection=None) # assigns reads to haplotypes

        return results_haps, posterior

    def get_haplotype_frequency_in_bulk(self, prior_haplotypes, reads, qualities, read_array_indices, count=False):
        ''' 
        Function to return haplotype frequency given a priory haplotypes 
        Needs prior haplotypes in a list (a la self.haps)
        Returns the count of the haplotypes in a bulk
        '''
        hap_prob_array = self.get_likelihood_naive(reads, prior_haplotypes, qualities)
        
        if count == True:
            posterior = self.estimate_posteriors_individual_reads(hap_prob_array, read_array_indices, selection=None) # assigns reads to haplotypes
            detected_haplotypes = set(posterior.values())
            results_haps = np.array([posterior.values().count(i) for i in range(len(self.all_H))])
        else:
            # print hap_prob_array.sum(axis=0)
            results_haps = hap_prob_array.sum(axis=0) / hap_prob_array.sum(axis=0).sum()
            posterior = self.estimate_posteriors_individual_reads(hap_prob_array, read_array_indices, selection=None) # assigns reads to haplotypes

        return results_haps, posterior    

    def iter_haplotype_count_a_priory(self, blocks, variants, variant_names, fragment_matrix, selection_threshold=3, count=True, return_vector = False):

        for blk in blocks:
            #start, end, prior_haplotypes

            read_array, read_array_indices = self.select_reads_per_bin(i, self.k, fragment_matrix)
            qualities = np.zeros((len(read_array), self.k))
            qualities.fill(30)
            results_haps, posterior = self.get_haplotype_frequency_in_bulk(prior_haplotypes, read_array, qualities, read_array_indices, count=count) 

            # for index, i in enumerate(prior_haplotypes):
                # print index, i, results_haps[index]

    def iter_haplotype_detection(self, variants, variant_names,fragment_matrix, selection_threshold=3, count=True, return_vector=False):
        '''Iterative binning over snp interval '''
        bins_dict = {}
        bins_posteriors = {}

        for i in range(len(variants) - self.k + 1):
            read_array, read_array_indices = self.select_reads_per_bin(i, self.k, fragment_matrix)
            #print len(read_array)
            qualities = np.zeros((len(read_array), self.k))
            qualities.fill(30)

            results_haps, posterior = self.get_hap_continous(read_array, qualities, read_array_indices, count=count) 
            real_haps = [self.all_H[index] for index, b in enumerate(results_haps) if b >= selection_threshold]
            
            if return_vector is False:
                bins_dict[i] = real_haps
            else:
                bins_dict[i] = results_haps

            bins_posteriors[i] = posterior

        return bins_dict, bins_posteriors

class haplotype_extension(helper_functions, likelihood_calc):
    '''
    If a interval is > 4 variants haplotype extension is done. 
    haplotype extension is non-deterministic, so it might produce the right solution, but doesn't have to.

    First a network is created based on overlap between blocks. 
    Conflicts are determined (i.e. one block or more contains wrong haplotypes)
    if there are conclicts the haplotype solution is split.        

    Then phasing is done using all possible paths and solution is returned. 

    '''
    def __init__(self, bins_temp, odds_list1, frg, likelihood_threshold, ploidy=4):
        self.bins_temp = bins_temp
        self.frg = frg
        self.oddslist = odds_list1
        self.likelihood_threshold = likelihood_threshold
        self.ploidy = ploidy

        # Delete the links which are not significant different from another solution. 
        for index in odds_list1:
            if odds_list1[index] < likelihood_threshold:
                # print (index), 'R', odds_list1[index], len(bins_temp[index][0])
                del self.bins_temp[index]
            else:
                pass
                # print (index), 'K', odds_list1[index], len(bins_temp[index][0])

    def stat_used_snps(self, bins, final, all_snps):
        ''' Calculate the number of snps used or lost in one step. '''

        all_snps_phased = []
        for index in bins:
            all_snps_phased = all_snps_phased + [data.names[snp] for snp in range(index, index +len(bins[index][0]))]
        
        all_snps_final = []
        for index in final:
            all_snps_final = all_snps_final + [data.names[snp] for snp in range(index, index +len(final[index][0]))]

        total = set(all_snps_phased + all_snps_final)
        #print total

        not_phased = [i for i in data.names if i not in total]

        # print len(set(all_snps_phased)), len(set(all_snps_final)), len(not_phased)
        
    def extension(self):
        '''
        Haplotype extension using pre-computed haploblocks of size k as input
        The module works, but is rudementary in computation speed. 
        The module consists out iterative joining of haplotype blocks until a pre-set confidence is used. 
        '''

        def get_flow(score_arr):
            haps = []
            for i in range(self.ploidy):
                k = np.where(score_arr[i] == 1)[0]
                for l in k:
                    haps += [[i, l]]

            combis = list(itertools.combinations(haps,self.ploidy))
            validity = False
            for i in combis:
                b1 = len(set([i[j][0] for j in range(self.ploidy)]))
                b2 = len(set([i[j][1] for j in range(self.ploidy)]))

                # b1 = len(set([i[0][0], i[1][0], i[2][0], i[3][0]]))
                # b2 = len(set([i[0][1], i[1][1], i[2][1], i[3][1]]))
                if b1 == self.ploidy and b2 == self.ploidy:
                    #print 'valid solution'
                    validity = True
            return validity

        def new_possible_link_get(score_arr):
            haps = []

            for i in range(self.ploidy):
                k = np.where(score_arr[i] == 1)[0]
                for l in k:
                    haps += [[i,l]]

            return haps

        def create_possible_solutions(paths):
            solutions = []

            for sol in itertools.combinations(paths, self.ploidy):
                bl1 = set([i[0] for i in sol])
                bl2 = set([i[1] for i in sol])
                #  If all nodes are targeted it is a valid solution
                if len(bl1) == self.ploidy and len(bl2) == self.ploidy:
                    solutions += [sol]
            return solutions

        def create_phasings(solutions, block_a, rest_b, block_1_indices, non_overlap):

            # we need to sort all phasings on occurence of indice as in this way they are placed like A> B, and it might also be A > B > A
            # if indice in block A, then take it, otherwise take it from B. 
                
            snp_indices = sorted(list(set(block_1_indices) | set(non_overlap)))

            hap_array = []
            for sol in solutions:
                sol_real = []
                for combi in sol:
                    #print combi

                    part1 = block_a[combi[0]].tolist()
                    part2 = rest_b[combi[1]].tolist()

                    haplotype = []

                    for snp_index in snp_indices:
                        if snp_index in block_1_indices:
                            s_i = block_1_indices.index(snp_index)
                            haplotype += [part1[s_i]]
                        else:
                            s_i = non_overlap.index(snp_index)
                            haplotype += [part2[s_i]]

                    # haplotype =  part1 + part2
                    
                    sol_real += [haplotype]
                
                if hap_array != []:
                    scores = [self.get_number_of_correct_haps(i, sol_real) for i in hap_array]
                    if max(scores) != self.ploidy:
                        hap_array += [sol_real]
                    #print scores
                else:
                    hap_array += [sol_real]

            return hap_array

        def select_reads_per_bin2(fragment_matrix, snp_indices, ungapped = True):
            
            '''Select reads in particular bin'''
            
            interval = len(snp_indices)

            bin_array = []
            bin_array_read_indices = []

            for index,read in enumerate(fragment_matrix[:, snp_indices]):
                count = read.tolist().count('-')
                #print count,
                if ungapped == True:
                    if count == 0:
                        bin_array += [read.tolist()]
                        bin_array_read_indices += [index]
                if ungapped == False:
                    #print read
                    if count <= interval -2:
                        #print read            #only for 4 snp?
                        bin_array += [read.tolist()]
                        bin_array_read_indices += [index]

            return bin_array, bin_array_read_indices

        def get_best_phasing(hap_array, snp_indices):
            '''
            It does assembly using the likelihood, but in a naive way. 
            At the end the posterior is estimated. We don't need the intermediate solutions.
            '''

            all_lm = []

            read_bin, read_bin_indices = select_reads_per_bin2(data.fragment_matrix, snp_indices, ungapped = False)

            qualities = np.zeros((len(read_bin), len(snp_indices)))
            qualities.fill(0.01)
            
            for solution in hap_array:
                #print solution
                hap_prob_array = self.get_likelihood_naive(read_bin, solution, qualities)
                LH = hap_prob_array.sum(axis=1)
                #print LH
                #print np.log(LH)
                prob = np.sum(np.log(LH))
                #print prob
                all_lm += [prob]


            all_lm = np.array(all_lm, dtype=np.longdouble)
            m = all_lm.min()
            total = np.log(np.sum(np.exp(all_lm - m))) + m

            MLE = all_lm.max()
            # report the only match, or if more than one match, report multiple matches. 
            #print all_lm
            if len(all_lm) != 1:
                #print all_lm
                indices_sel = np.where(all_lm == all_lm.max())[0]
                if len(indices_sel) == 1:
                    selection = int(indices_sel)    
                    idxs = np.delete(all_lm, selection, axis=0)
                    MLE2 = idxs.max()
                    ratio = np.exp(MLE - MLE2)
                else:
                    #print '\t', indices_sel, all_lm
                    selection = 0
                    ratio = 1
            else:
                selection = 0
                ratio = 10000

            return hap_array[selection], ratio
        
        def phase_two_blocks(hap_array, snp_indices):
            '''Phases the haplotypes given a set of phasings, if not possible, return False'''
        
            if len(hap_array) == 1:
                sol = hap_array[0]
                ratio = 50000
                return sol, ratio
            else:
                #print hap_array
                sol, ratio = get_best_phasing(hap_array, snp_indices)

                if ratio > self.likelihood_threshold:
                    ratio = 0
                    return sol, ratio
                else:
                    return False, False

        def overlapping_bins_determination(bins, overlap=3):
            ''' 
            Get overlapping bins based on dictionary with blocks
            If no overlap, report adjacent bins. 
            '''

            interval = sorted(bins.keys())
            overlapping_intervals = []
            non_overlapping_intervals = []
            overlaps = []

            for index, i in enumerate(interval):
                if len(interval) > index+1: 
                    prev_index = index
                    next_index = index + 1

                    a, b = interval[prev_index], interval[next_index]

                    t = range(a, a + len(bins[a][0]))
                    t2 = range(b, b + len(bins[b][0]))
                    #print t, t2
                    cnt = 0
                    for i in t:
                        if i in t2:
                            cnt +=1

                    #print a, b, cnt
                    if cnt >= overlap:
                        overlapping_intervals += [[interval[prev_index], interval[next_index]]]
                        overlaps += [cnt] 
            return overlapping_intervals, overlaps

        def get_non_overlaps(overlapping_intervals, all_indices):

            all_used = []
            for i in overlapping_intervals:
                all_used = all_used  + i
            all_used = list(set(all_used))

            not_overlap = []
            for i in all_indices:
                if i not in all_used:
                    not_overlap += [i]
            return not_overlap

        def score_overlap(overlap_a, overlap_b):
            score_arr2 = np.zeros((self.ploidy,self.ploidy))

            for ind_first, i in enumerate(overlap_a):
                for ind_second, j in enumerate(overlap_b):
                    #print i, j
                    if i.tolist() == j.tolist():
                        score_arr2[ind_first][ind_second] += 1
            return score_arr2

        def one_block_extension(block1, block2, index, block1_indices, block2_indices):
            ''' 
            Two blocks as input. Return dictionary of blocks and their assocciated indices. 
            Can be used for any number of two blocks, also blocks that are far apart. 

            A+B > AB
            A+B > A + B
            A-B-A > ABA

            '''

            overlap = sorted(list(set(block1_indices) & set(block2_indices)))
            snp_indices = sorted(list(set(block1_indices) | set(block2_indices)))
            non_overlap_block2 = sorted([i for i in block2_indices if i not in block1_indices])

            if overlap != []:
                block_1_ind = [block1_indices.index(o) for o in overlap]
                block_2_ind = [block2_indices.index(o) for o in overlap]

                overlap_a =  np.array(block1)[:,block_1_ind]
                overlap_b =  np.array(block2)[:,block_2_ind]
                
                score_array = score_overlap(overlap_a, overlap_b)
                # Create the blocks that need to be combined!
                block_a = np.array(block1)
                block_b = np.delete(np.array(block2), block_2_ind, 1)
                    #print score_array
            else:
                # if the overlap is 0. 
                snp_indices = sorted(list(set(block1_indices) | set(block2_indices)))
                block_a = np.array(block1)
                block_b = np.array(block2)
                score_array = np.ones((self.ploidy,self.ploidy))
            
            validity = get_flow(score_array)

            if validity == True:
                paths_new = new_possible_link_get(score_array) #get new paths. 
                solutions = create_possible_solutions(paths_new) # Create all possible solutions

                hap_array = create_phasings(solutions, block_a, block_b, block1_indices, non_overlap_block2) # Create all real possible distinct phasings

                # print hap_array 

                sol, ratio = phase_two_blocks(hap_array, snp_indices)

                # if confidence in a solution add to the bins_temp, if not add first block?
                if sol != False:
                    bins = {index[0]: sol}
                    snp_indices_bin = {index[0]: snp_indices}
                    return bins, snp_indices_bin
            
            bins = {index[0]:block1, index[1]:block2}
            snp_indices_bin = {index[0]:block1_indices, index[1]:block2_indices}
            
            return bins, snp_indices_bin
                    
        def recursive_phasing2(bins, new_bins_snp_indices = False):

            final_bin_list = {}
            final_confflicts = {} 

            new_bins = bins
            
            #SNP indices per block (can be 2, 3, 4 long)
            if new_bins_snp_indices == False:
                new_bins_snp_indices = {}
                for block in new_bins:
                    new_bins_snp_indices[block] = range(block, block+len(new_bins[block][0]))

            # the snp indices are updated during extension. 
            # for each overlap try to phase
            # print 'number of blocks start', len(new_bins)
            # The start is new_bins in which all bins are location. 
            # We have for each iteration a list of possible overlaps. 
            # For each overlap we combine two blocks and return one block if possible. 
            # If not we return two blocks. If the block is already used, discard it. 
            succes = False
            iteration = 0 

            already_tried_intervals = []

            while succes== False:
                #print
                overlapping, overlaps = overlapping_bins_determination(new_bins, overlap=0)
                not_overlap = get_non_overlaps(overlapping, new_bins.keys())            
                #print 'non_ovverlapping' , not_overlap
                #print 'overlaps', overlapping
                
                phased_blocks = []
                temp_bins = {}
                temp_indices = {} 

                for block in not_overlap:
                    temp_bins[block] = new_bins.pop(block)
                    temp_indices[block] = new_bins_snp_indices.pop(block)
                
                total_phased = 0
                
                for interval in overlapping:
                    new_interval = '_'.join([str(m) for m in interval])
                    
                    if interval in already_tried_intervals:
                        continue

                    block_1 = new_bins[interval[0]]
                    block_2 = new_bins[interval[1]]
                    block_1_ind = new_bins_snp_indices[interval[0]]
                    block_2_ind = new_bins_snp_indices[interval[1]]

                    bins, snp_indices = one_block_extension(block_1, block_2, interval, block_1_ind, block_2_ind)

                    if len(bins) == 1:
                        for bl in bins:
                            temp_bins[bl] = bins[bl]
                            temp_indices[bl] = snp_indices[bl]
                            phased_blocks = phased_blocks + interval
                            total_phased += 1

                for bl in new_bins:
                    if bl not in phased_blocks:
                        temp_bins[bl] = new_bins[bl]
                        temp_indices[bl] = new_bins_snp_indices[bl]

                # Determine proportion of overlaps that are phased
                # print 'niteration %i' %(iteration)

                # print 'overlaps', len(overlapping)
                # print 'phased total', total_phased
                # print 'remaining blocks', len(temp_bins)

                iteration+=1
                #succes = True
                if total_phased == 0:
                    succes = True
                    
                new_bins = temp_bins
                new_bins_snp_indices = temp_indices

            return new_bins, new_bins_snp_indices

        def determine_read_bridges(indices_first_block, indices_second_block):

            read_bin1, read_bin_indices1 = select_reads_per_bin2(data.fragment_matrix, indices_first_block, ungapped = False)
            read_bin2, read_bin_indices2 = select_reads_per_bin2(data.fragment_matrix, indices_second_block, ungapped = False)
            overlap = sorted(list(set(read_bin_indices1) & set(read_bin_indices2)))

            return overlap

        def get_selected_combis(new_bins, new_bins_indices, combination_keys=False):
            
            if combination_keys==False:
                bin_keys = sorted(new_bins.keys())
                combinations_blocks = list(itertools.combinations(bin_keys, 2))
            else:
                combinations_blocks = combination_keys

            blocks_used = []
            selected_combis = []

            for index, i in enumerate(combinations_blocks):
                overlap = determine_read_bridges(new_bins_indices[i[0]], new_bins_indices[i[1]])
                if len(overlap) > 0:
                    #print index, i, distances_between_blocks[index], len(overlap)
                    selected_combis += [list(i)]
                    blocks_used = blocks_used + list(i)

            return selected_combis, blocks_used

        def recursive_bridging(binsa, indicesa):
            no_bins = len(binsa)
            bin_keys = sorted(binsa.keys())
            
            # Find haplotype solutions that have reads spanning. 

            new_bins = binsa
            new_bins_snp_indices = indicesa


            starts = 0
            while starts < 50 and starts < len(new_bins):
                new_bins, new_bins_indices = bridge_one_to_all(new_bins, new_bins_snp_indices, starts)
                starts +=1


            return new_bins, new_bins_indices
        
        def bridge_one_to_all(new_bins, new_bins_snp_indices, start):


            outer = False
            iteration = 0 

            start_interval = start
            # print '####', start_interval
            first_interval = start_interval
            while outer == False:

                # start loop until no combis anymore
                
                new_bins_keys = sorted(new_bins.keys())
                u = new_bins_keys.pop(start_interval)
                first_interval = u
                combis = [sorted([u,y]) for y in new_bins_keys]
                
                selected_combis, blocks_used = get_selected_combis(new_bins, new_bins_snp_indices)
                combis = [i for i in combis if i in selected_combis]
                
                inner = False
                # Inner loop to get a valid phasing from this u block to others. 
                #print u, combis
                
                while inner == False:
                    
                    if combis == []:
                        innner = True
                        outer = True
                        break

                    interval = combis.pop(0)
                    new_interval = '_'.join([str(m) for m in interval]) # unique interval name per iteration
                    
                    block_1 = new_bins[interval[0]]
                    block_2 = new_bins[interval[1]]
                    block_1_ind = new_bins_snp_indices[interval[0]]
                    block_2_ind = new_bins_snp_indices[interval[1]]

                    bins, snp_indices = one_block_extension(block_1, block_2, interval, block_1_ind, block_2_ind)

                    if len(bins) == 1:
                        # print u, interval, len(snp_indices[interval[0]]), 'y'
                        new_bins[interval[0]] = bins[interval[0]]
                        new_bins_snp_indices[interval[0]] = snp_indices[interval[0]]
                        first_interval = interval[0]
                        del new_bins[interval[1]]
                        del new_bins_snp_indices[interval[1]]
                        
                        inner = True
                    else:
                        #print u, interval, len(snp_indices[interval[0]]), 'n'
                        if interval == []:
                            inner = True

            # print new_bins_snp_indices[first_interval]
            return new_bins, new_bins_snp_indices

        def phase_blocks(overlapping, new_bins, overlaps, used_block_indices, temp_indices_blocks):
            '''
            Phasing of all blocks that are adjacent to eachother. 
            Note that adjacent does not imply that they cannot skip SNPs.
            the blocks might have overlap

            '''
            # print temp_indices_blocks

            bins_temp = {}
            conflict_bins = []

            for index, i in enumerate(overlapping):
                if i in used_block_indices:
                    continue


                indices_a = range(i[0], i[0]+ len(new_bins[i[0]][0]))
                indices_b = range(i[1], i[1]+ len(new_bins[i[1]][0]))

                # Get the indices of each block to be used in comparison
                overlap = sorted(list(set(indices_a) & set(indices_b)))
                
                if overlap != []:
                    snp_indices = sorted(list(set(indices_a) | set(indices_b)))

                    block_a_ind = []
                    block_b_ind = []
                    

                    for o in overlap:
                        block_a_ind += [indices_a.index(o)]
                        block_b_ind += [indices_b.index(o)]

                    # Determine the score array

                    overlap_a =  np.array(new_bins[i[0]])[:,block_a_ind]
                    overlap_b =  np.array(new_bins[i[1]])[:,block_b_ind]

                    # Create the blocks that need to be combined!
                    
                    block_a = np.array(new_bins[i[0]])
                    rest_b = np.delete(np.array(new_bins[i[1]]), block_b_ind, 1)

                    score_array = score_overlap(overlap_a, overlap_b)
                    #print score_array
                    validity = get_flow(score_array)
                else:
                    # if the overlap is 0. 
                    snp_indices = sorted(list(set(indices_a) | set(indices_b)))
                    block_a = np.array(new_bins[i[0]])
                    rest_b = np.array(new_bins[i[1]])
                    score_array = np.ones((self.ploidy,self.ploidy))
                    #print score_array
                    validity = get_flow(score_array)

                #print 'validity', validity

                if validity == True:
                    paths_new = new_possible_link_get(score_array) #get new paths. 

                    # Create all possible solutions
                    solutions = create_possible_solutions(paths_new)

                    # Create all real possible distinct phasings
                    hap_array = create_phasings(solutions, block_a, rest_b)
                    #print(len(hap_array))
                    # Do the actual phasing
                    sol, ratio = phase_two_blocks(hap_array, snp_indices)

                    # if confidence in a solution add to the bins_temp, if not add first block?
                    if sol != False:
                        bins_temp[i[0]] = sol
                        print i, ratio
                        # Save which indices are used. 
                        temp_indices_blocks = list(set(temp_indices_blocks + i))

                    else:
                        conflict_bins = conflict_bins + snp_indices
                        print i, ratio
                        # If the LR is not high enough, this doesn't mean the block should be discarded.  
                        # It can be added to the bins_temp and kept for further iterations. 

                        used_block_indices += [i]


                        if i[0] not in temp_indices_blocks:
                            bins_temp[i[0]] = new_bins[i[0]]

                        if i[1] not in temp_indices_blocks:
                            bins_temp[i[1]] = new_bins[i[1]]
                else:
                    conflict_bins = conflict_bins + snp_indices

            return bins_temp, conflict_bins, used_block_indices, temp_indices_blocks

        
        # Here the above programmed functions are used. We should make these object-oriented, as that is easier to adapt. For now it works
        # We can both perform extension, and bridging (jumping). The bridging step takes a long time. 
        # The extension step does allow a overlap of 0, but NOT -1, so no jumping between blocks over snps here. All blocks should therefore be adjacent. 

        bins_temp, new_bins_snp_indices  = recursive_phasing2(self.bins_temp)

        if args.br == True:
            bins_temp, new_bins_snp_indices = recursive_bridging(bins_temp, new_bins_snp_indices)
            #bins_temp, new_bins_snp_indices  = recursive_phasing2(bins_temp, new_bins_snp_indices=new_bins_snp_indices)


        def format_bins(bins, new_bins_snp_indices):
            ind_block = {}
            indices = {} 
            all_snps_phased = []

            for index in bins_temp:
                ind_block[index] = [data.names[snp] for snp in sorted(new_bins_snp_indices[index])]
                indices[index] = [snp for snp in sorted(new_bins_snp_indices[index])] 

                all_snps_phased = all_snps_phased + [data.names[snp] for snp in range(index, index +len(bins_temp[index][0]))]
            
            return ind_block, indices, all_snps_phased

        b.ind_block, b.indices, b.all_snps_phased = format_bins(bins_temp, new_bins_snp_indices)

        b.hap = bins_temp
    
if __name__ == "__main__":

    description_text = 'Processing of fragment matrices into haplotypes.'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    #parser.add_argument('-i', type=str,help='Input fragment file, if - then read from stdin')
    parser.add_argument('-o', type=str, default='-', help='Output bins file name, if nothing is specified then put to stdout' )
    parser.add_argument('-interval_length', type=int, default=4, help='Length of the interval used for haplotyping, default is 4')
    parser.add_argument('-bins_file', type=str, default='-', help='input binsfile')   
    parser.add_argument('-ploidy', type=int, default=4, help='ploidy level')
    parser.add_argument('-HQ', type=int, default=50, help='Theshold for individual blocks to consider if extension is provided')
    parser.add_argument('-skip', type=int, default=1, help='Overlapping haplotype blocks or not')

    # Arguments that are needed in the new version

    parser.add_argument('-i', default='-', type=str,help='Input fragment file, if - then read from stdin')
    parser.add_argument('-vcf', default='-', type=str,help='Input fragment file, if - then read from stdin')
    parser.add_argument('-bed', default='-', type=str,help='Input fragment file, if - then read from stdin')


    # arguments that are switched on or off depending on presence
    parser.add_argument('-p', action='store_true', help='pooled data from different cultivars')
    parser.add_argument('-d', action='store_true', help='Estimate dosages of haplotypes, instead a prior input of genotype calls')
    parser.add_argument('-e', action='store_true', help='Perform extension')
    parser.add_argument('-br', action='store_true', help='Perform Bridging')

    parser.add_argument('-savereads', action='store_true', help='save reads per haplotype')
    parser.add_argument('-v', action='store_true', help='save probabilities')
    parser.add_argument('-b', action='store_true', help='return counts based on prior_input_haplotypes')
    parser.add_argument('-n', action='store_true', help='naive extension (probablistic) or based on heuristic rules default is heuristic')

    args = parser.parse_args()

    # Read from fragment data from file in intervals


    def re_insert_homozygous_snps(homozygous_indices, heterozygous_indices, bin, indices, homo_bin, all_snps, all_names):

        # all_names = complete set of names for all snps

        # construct H
        H = np.array(bin).T.tolist()

        # Get the SNP indices before the snp selection during pre-processing. 
        old_het_indices_in_block = [heterozygous_indices[i] for i in indices] 

        new_hap_dict = {}
       
        # for each heterozygous snp, determine if there are adjacent homozygous snps
        
        for new_snp_index, old_snp_index in enumerate(old_het_indices_in_block):
            # Save hapvector in new_hap_dict
            new_hap_dict[old_snp_index] = H[new_snp_index]

            add = 1
            while True:
                # add to new_hap if homozygous
                n = old_snp_index + add
                if n in homozygous_indices:
                    n_ind = homozygous_indices.index(n)
                    new_hap_dict[n] = homo_bin[n_ind]
                    add +=1
                else:
                    break

            add = 1
            while True:
                # add to new_hap if homozygous
                n = old_snp_index - add

                if n in homozygous_indices:
                    n_ind = homozygous_indices.index(n)
                    new_hap_dict[n] = homo_bin[n_ind]
                    add +=1
                else:
                    break

        # At this point the new_hap_dict should contain both the homozygous as the heterozygous indices. 
        # We can assume that the VCF file is sorted and therefore the indices can be sorted and we can add the names. 

        haplotypes = []
        new_names = []

        for index in sorted(new_hap_dict.keys()):
            haplotypes += [new_hap_dict[index]]
            new_names += [all_names[index]]

        return new_names, np.array(haplotypes).T.tolist()

    a = extract_interval(args.i, args.vcf, args.bed)
    bed_intervals = a.get_bed_intervals()


    if bed_intervals != []:
        for (interval, interval2) in bed_intervals:

            try:
                # print interval, interval2

                # Extract snp information as well as read from samfile
                snp_information =    a.extract_vcf(interval2)
                read_information = a.extract_frg(interval, bed=True)

                if len(snp_information) == 0 or len(read_information) == 0:
                    continue

                # Create the fragment matrix on the fly and also retain the homozygous snps
                # To do: Fix error if no heterozygous snp is present. 
                data = read_data2(snp_information, read_information, args.ploidy)


                # Determine if the interval_length is possible, otherwise we 
                if data.k < args.interval_length:
                    interval_length = data.k
                else:
                    interval_length = args.interval_length

                # if only one snp is present we have a special case, we can just let the dosage in there, but there might be additional homozygous snps as well! 
                
                if interval_length == 1:

                    try:
                        bin = [0] * args.ploidy
                        for i in range(int(data.variants[0])):
                            bin[i] = 1
                    except:
                        bin = ['.'] * args.ploidy

                    oddslist = {}
                    oddslist[0] = 100
                    temp_hap = np.array([str(i) for i in bin]).reshape((args.ploidy,1))

                    index_block = 0
                    indices = [0]
                    new_names, new_H = re_insert_homozygous_snps(data.initial_homozygous_snps_indices, data.initial_het_snps_indices ,temp_hap , indices, data.bins_homozygous, data.all_snps, data.all_snps_id)
                    write_bins(args.o, new_H, new_names, index_block)
                    continue
                    
                #Do local haplotyping
                short_haplotyping = ML_haplotyping(interval_length=args.interval_length, dosage_estimation=args.d, ploidy=args.ploidy)
                bins, oddslist, bin_array_list, posterior_dict = short_haplotyping.iterative_binning(data.variants,data.fragment_matrix, args.HQ, data.location, overlap=args.skip)

                # Perform haplotype extension
                if args.e and len(bins) > 1:
                    b = haplotype_extension(bins, oddslist, data.fragment_matrix, args.HQ, ploidy=args.ploidy)
                    
                    if args.n:
                        b.iter_naive_extension()
                    else:
                        b.extension()
                    
                    # add bridging
                    # Save all blocks. Here we need to insert the homozygous snps. 
                    # The homozygous snps are in data.homo

                    for index in sorted(b.hap.keys()):
                        temp_hap = b.hap[index]

                        # insert the removed snps
                        new_names, new_H = re_insert_homozygous_snps(data.initial_homozygous_snps_indices, data.initial_het_snps_indices , b.hap[index], b.indices[index], data.bins_homozygous, data.all_snps, data.all_snps_id)

                        write_bins(args.o, new_H, new_names, index)


                else:
                    # Temporary fix to get the right snps at the right place!
                    for index in oddslist:
                        if oddslist[index] < args.HQ:
                            del bins[index]

                    # Here format the SNPS
                    
                    ind_block = {}
                    indices = {}
                    for index in bins:
                        # print 'range', range(index, index +len(bins[index][0]))
                        # print 'datanames' , data.names
                        ind_block[index] = [data.names[snp] for snp in range(index, index +len(bins[index][0]))]
                        indices[index] = [snp for snp   in range(index, index +len(bins[index][0]))] 

                    # Write bins, but also include homozygous snps. 
                    for index in sorted(bins.keys()):
                        temp_hap = bins[index]
                        temp_names = ind_block[index]

                        new_names, new_H = re_insert_homozygous_snps(data.initial_homozygous_snps_indices, data.initial_het_snps_indices , temp_hap, indices[index], data.bins_homozygous, data.all_snps, data.all_snps_id)
                        write_bins(args.o, new_H, new_names, index)
            except:
                print '!!Error in this interval!!'
    else:
        snp_information = a.extract_vcf(None, bed=False)
        read_information = a.extract_frg(None, bed=False)
        data = read_data2(snp_information, read_information)
        
        #print read_information

        if len(snp_information) == 0 or len(read_information) == 0:
            pass
        else:
            do_haplotyping(snp_information, read_information)
