#!/home/johan/anaconda2/bin/python

import argparse
import os

description_text = 'Processing of fragment matrices into haplotypes.'
epilog_text = 'Copyright Johan Willemsen May 2016'

parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
parser.add_argument('-i', type=str, help='Input vcffile, if - then read from stdin')
parser.add_argument('-t', type=str, help='Prefix')
parser.add_argument('-o', type=str, default='-', help='output reference fasta, if - then read from stdin')

args = parser.parse_args()

def rename(vcf_file, output_file):
    '''
    Give all snps a name
    '''
    vcf = open(vcf_file, 'r')
    vcf_filtered = open(output_file, 'wb')
    count_f = 0
    total = 0
    for row in vcf:
        if '#' in row:
            vcf_filtered.write(row)
        else:
            try:
                cnt = row.split('\t')
                if cnt[2] == '.':
                    cnt[2] = '%s_%i' % (args.t, count_f+1)
                    
                if cnt[3] == '.':
                    continue
                                       
                vcf_filtered.write('\t'.join(cnt))
                count_f +=1
            except:
                continue
    vcf.close()
    vcf_filtered.close()

rename(args.i, args.o)



