#!/bin/bash

# Setup locations of software used in the script below
tabix_location=""
haplotype_scripts_location="./haplotype_assembly_bin"
bedtools_location=""
samtools_location=""
python_location=""

export PATH="$tabix_location:$PATH"
export PATH="$haplotype_scripts_location:$PATH"
export PATH="$bedtools_location:$PATH"
export PATH="$samtools_location:$PATH"
export PATH="$python_location:$PATH"

BAM=$1  # BAM file
VCF=$2  # VCF file
BED=$3  # BED file
p=$4    # ploidy
CV=$5   # CV name

# Processing of bam to frg

python $haplotype_scripts_location/processing.py -i $BAM -vcf $VCF -bed $BED -o $CV.frg -tech PE;

sort -k1V -k2n -k3n $CV.frg > $CV.sorted.frg    # Sort fragments on location
bgzip -f $CV.sorted.frg # Compress frg file
tabix -s 1 -b 2 -e 3 $CV.sorted.frg.gz # index compressed frg file
#rm $CV.frg

# move files to fragment files
mv $CV.frg ./fragment_files/
mv $CV.sorted.frg.gz ./fragment_files/
mv $CV.sorted.frg.gz.tbi ./fragment_files/

# # # # Haplotyping
python $haplotype_scripts_location/local_haplotyping.py -i ./fragment_files/$CV.sorted.frg.gz -vcf $VCF -bed $BED -d -e -ploidy $p -o $CV.bins

mv $CV.bins ./bins_files/
mv $CV.bed ./bins_files
rm interval_VCF.vcf.bed

